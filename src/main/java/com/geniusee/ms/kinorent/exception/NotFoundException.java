package com.geniusee.ms.kinorent.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }

    public static void throwItWithMessage(String message) {
        throw new NotFoundException(message);
    }
}
