package com.geniusee.ms.kinorent.exception;

public class RetryProcessException extends RuntimeException {
    public RetryProcessException(String message) {
        super(message);
    }

    public RetryProcessException() {
    }

    public static void throwIt() {
        throw new RetryProcessException();
    }

    public static void throwItWithMessage(String message) {
        throw new RetryProcessException(message);
    }
}
