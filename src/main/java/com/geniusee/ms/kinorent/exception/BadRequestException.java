package com.geniusee.ms.kinorent.exception;

public class BadRequestException extends RuntimeException {
    public BadRequestException(String message) {
        super(message);
    }

    public static void throwItWithMessage(String message) {
        throw new BadRequestException(message);
    }
}
