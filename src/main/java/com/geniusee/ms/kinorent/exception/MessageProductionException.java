package com.geniusee.ms.kinorent.exception;

public class MessageProductionException extends RuntimeException {
    public MessageProductionException(String message) {
        super(message);
    }

    public MessageProductionException() {
    }

    public static void throwIt() {
        throw new MessageProductionException();
    }

    public static void throwItWithMessage(String message) {
        throw new MessageProductionException(message);
    }
}
