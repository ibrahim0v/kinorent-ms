package com.geniusee.ms.kinorent.repository;

import com.geniusee.ms.kinorent.entity.OrdersBulk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrdersBulkRepository extends JpaRepository<OrdersBulk, Integer> {
    List<OrdersBulk> findAllByOrderReferenceId(String orderReferenceId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update OrdersBulk b set b.orderStatus =:orderStatus "
            + "where b.kafkaReferenceId =:kafkaReferenceId")
    void updateFromKafkaResponse(@Param("kafkaReferenceId") String kafkaReferenceId,
                                 @Param("orderStatus") String orderStatus);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update OrdersBulk b set b.orderStatus =:orderStatus where b.kafkaReferenceId "
            + "=:kafkaReferenceId")
    void updateProcessStatus(@Param("kafkaReferenceId") String kafkaReferenceId,
                             @Param("orderStatus") String orderStatus);

    @Query("select kafkaReferenceId from OrdersBulk where title =:title and "
            + "orderReferenceId =:orderReferenceId and orderStatus in ('ERROR', 'NEW')")
    Optional<String> findRefIdByTitleAndCorId(@Param("title") String title, @Param(
            "orderReferenceId") String orderReferenceId);

    @Query("select log from OrdersBulk log where log.kafkaReferenceId =:kafkaReferenceId and "
            + "log.orderStatus =:orderStatus")
    Optional<OrdersBulk> findLogEntryByKafkaReferenceId(@Param("kafkaReferenceId") String kafkaReferenceId,
                                                        @Param("orderStatus") String orderStatus);

}
