package com.geniusee.ms.kinorent.mapper;

import com.geniusee.ms.kinorent.dto.kafka.OrderDetailsKafkaDto;
import com.geniusee.ms.kinorent.dto.request.MovieDto;
import com.geniusee.ms.kinorent.dto.request.OrderDetailsRequestDto;
import com.geniusee.ms.kinorent.dto.response.OrderStatusDto;
import com.geniusee.ms.kinorent.entity.OrdersBulk;
import com.geniusee.ms.kinorent.mapper.qualifier.OrderCreationHelperMapper;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        imports = {java.util.UUID.class},
        uses = {OrderCreationHelperMapper.class}
)
public interface OrderCreationMapper {

    @Mapping(target = "orderDetails", source = "movieDto", qualifiedByName = "mapDetailsToClob")
    OrdersBulk toBulk(MovieDto movieDto);

    OrderDetailsKafkaDto toDetailsKafkaDto(MovieDto movieDto);

    @Mapping(target = "kafkaReferenceId", expression = "java(UUID.randomUUID().toString())")
    void enrichBulkEntryWithRequestDetails(OrderDetailsRequestDto movieRequestDto,
                        @MappingTarget OrdersBulk ordersBulk);

    List<OrderStatusDto> toStatusDtoList(List<OrdersBulk> ordersBulks);

    @Mapping(target = "status", source = "orderStatus")
    OrderStatusDto toStatusDto(OrdersBulk ordersBulk);
}


