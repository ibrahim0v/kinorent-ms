package com.geniusee.ms.kinorent.mapper.qualifier;

import com.geniusee.ms.kinorent.dto.request.MovieDto;
import com.google.gson.GsonBuilder;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderCreationHelperMapper {

    @Named("mapDetailsToClob")
    public String mapDetailsToClob(MovieDto movieDto) {
        var gson = new GsonBuilder()
                .serializeNulls()
                .create();
        return gson.toJson(movieDto);
    }

}
