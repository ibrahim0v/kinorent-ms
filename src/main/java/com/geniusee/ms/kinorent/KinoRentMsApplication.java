package com.geniusee.ms.kinorent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KinoRentMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(KinoRentMsApplication.class);
    }

}
