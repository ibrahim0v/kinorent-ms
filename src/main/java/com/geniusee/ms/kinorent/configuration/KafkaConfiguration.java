package com.geniusee.ms.kinorent.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.listener.LoggingErrorHandler;
import org.springframework.kafka.support.converter.ByteArrayJsonMessageConverter;

@Configuration
@EnableKafka
public class KafkaConfiguration {
    @Bean
    public LoggingErrorHandler errorHandler() {
        return new LoggingErrorHandler();
    }

    @Bean
    public ByteArrayJsonMessageConverter jsonConverter() {
        return new ByteArrayJsonMessageConverter();
    }

}
