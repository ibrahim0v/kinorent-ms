package com.geniusee.ms.kinorent.messagefactory;

import com.geniusee.ms.kinorent.dto.request.MovieDto;

public interface MessageProducer {
    void produceMessage(MovieDto msgDetails, String orderReferenceId);
}
