package com.geniusee.ms.kinorent.messagefactory;

import com.geniusee.ms.kinorent.dto.kafka.OrderKafkaDto;
import com.geniusee.ms.kinorent.entity.OrdersBulk;
import com.geniusee.ms.kinorent.logger.MainLogger;
import com.geniusee.ms.kinorent.repository.OrdersBulkRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MessageConsumer {
    private static final MainLogger LOGGER = MainLogger.getLogger(MessageConsumer.class);
    private final OrdersBulkRepository ordersBulkRepository;

    @KafkaListener(topics = "${kafka.topic.create-order}", groupId = "${spring.kafka.consumer.group-id}")
    public void listenKafka(@Payload OrderKafkaDto orderResponseKafka) {
        String processStatus = "OK";
        var movieKafka = orderResponseKafka.getOrderDetailsKafkaDto();
        var kafkaReferenceId = orderResponseKafka.getReferenceId();
        LOGGER.info("Updating table from consumed kafka data: {}, {}, {}, {}, {}", kafkaReferenceId, processStatus);
        updateBulkFromKafka(kafkaReferenceId, processStatus);
    }

    private Optional<OrdersBulk> getLogEntryByKafkaReferenceId(String kafkaReferenceId) {
        try {
            return ordersBulkRepository.findLogEntryByKafkaReferenceId(kafkaReferenceId, "OK");
        } catch (Exception ex) {
            LOGGER.error("Getting log from database by orderReferenceId has failed",
                    ex.getMessage());
            return Optional.empty();
        }
    }

    private void updateBulkFromKafka(String referenceId, String status) {
        try {
            ordersBulkRepository.updateFromKafkaResponse(referenceId, status);
        } catch (Exception ex) {
            LOGGER.error("Updating log table has been failed due to database error", ex.getMessage());
        }
    }
}
