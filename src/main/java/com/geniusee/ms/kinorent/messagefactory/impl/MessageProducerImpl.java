package com.geniusee.ms.kinorent.messagefactory.impl;

import com.geniusee.ms.kinorent.constant.ExceptionMessage;
import com.geniusee.ms.kinorent.constant.OperationStatus;
import com.geniusee.ms.kinorent.dto.kafka.OrderKafkaDto;
import com.geniusee.ms.kinorent.dto.request.MovieDto;
import com.geniusee.ms.kinorent.exception.MessageProductionException;
import com.geniusee.ms.kinorent.exception.RetryProcessException;
import com.geniusee.ms.kinorent.logger.MainLogger;
import com.geniusee.ms.kinorent.mapper.OrderCreationMapper;
import com.geniusee.ms.kinorent.messagefactory.MessageProducer;
import com.geniusee.ms.kinorent.repository.OrdersBulkRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class MessageProducerImpl implements MessageProducer {
    private static final MainLogger LOGGER = MainLogger.getLogger(MessageProducerImpl.class);
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final OrderCreationMapper orderCreationMapper;
    private final OrdersBulkRepository ordersBulkRepository;
    @Value("${kafka.topic.create-order}")
    private String createOrderTopic;

    @Override
    public void produceMessage(MovieDto msgDetails, String orderReferenceId) {
        var message = prepareMessage(msgDetails, orderReferenceId);
        sendMessage(createOrderTopic, message);
        setOrderStatusByKafkaReferenceId(message.getReferenceId(), OperationStatus.IN_PROGRESS);
    }

    private OrderKafkaDto prepareMessage(MovieDto msgDetails, String orderReferenceId) {
        var movieKafkaDto = orderCreationMapper.toDetailsKafkaDto(msgDetails);
        var title = msgDetails.getTitle();
        var kafkaReferenceId = getKafkaReferenceId(title, orderReferenceId);

        if (kafkaReferenceId.isPresent()) {
            return OrderKafkaDto.of(kafkaReferenceId.get(), movieKafkaDto);
        } else {
            throw new RetryProcessException("All records are in progress");
        }
    }

    private void sendMessage(String topic, OrderKafkaDto msg) {
        LOGGER.info("Sending message {} to consumer with topic {}", msg, topic);
        kafkaTemplate.send(topic, msg).addCallback(
                new ListenableFutureCallback<>() {
                    @Override
                    public void onFailure(Throwable ex) {
                        LOGGER.error("Message production has failed", ex.getMessage());
                    }

                    @Override
                    public void onSuccess(SendResult<String, Object> result) {
                        LOGGER.info("Message has been successfully produced");
                    }
                });
    }

    private Optional<String> getKafkaReferenceId(String title, String orderReferenceId) {
        try {
            return ordersBulkRepository.findRefIdByTitleAndCorId(title, orderReferenceId);
        } catch (Exception ex) {
            LOGGER.error("Getting referenceId from database has failed for producing to kafka", ex.getMessage());
            return Optional.empty();
        }
    }

    private void setOrderStatusByKafkaReferenceId(String kafkaReferenceId, String status) {
        try {
            ordersBulkRepository.updateProcessStatus(kafkaReferenceId, status);
        } catch (Exception ex) {
            LOGGER.error("Setting process status 'IN_PROGRESS' has failed due to database error: ",
                    ex.getMessage());
            MessageProductionException.throwItWithMessage(ExceptionMessage.MSG_PROD_FAIL);
        }
    }
}

