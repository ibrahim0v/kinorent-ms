package com.geniusee.ms.kinorent.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MS_KINORENT_ORDERS",
        uniqueConstraints = {@UniqueConstraint(columnNames = "ORDER_REFERENCE_ID")})
public class OrdersBulk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ORDER_REFERENCE_ID")
    private String orderReferenceId;

    @Column(name = "KAFKA_REFERENCE_ID")
    private String kafkaReferenceId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "STATUS")
    private String orderStatus = "NEW";

    @Column(name = "ORDER_DETAILS")
    private String orderDetails;

    @Column(name = "CREATE_TIME")
    private LocalDateTime createTime;

    @Column(name = "USER_ID")
    private String userId;
}
