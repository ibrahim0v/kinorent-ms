package com.geniusee.ms.kinorent.service;

import com.geniusee.ms.kinorent.dto.request.OrderDetailsRequestDto;
import com.geniusee.ms.kinorent.dto.response.OrderCreationResponseDto;
import com.geniusee.ms.kinorent.dto.response.OrdersStatusResponseDto;

public interface OrderCreationService {
    OrderCreationResponseDto createBulkOrders(OrderDetailsRequestDto movieRequestDto, String userId);

    OrdersStatusResponseDto consumeCreatedOrders(String orderReferenceId);
}
