package com.geniusee.ms.kinorent.service.impl;

import com.geniusee.ms.kinorent.constant.ExceptionMessage;
import com.geniusee.ms.kinorent.constant.OperationStatus;
import com.geniusee.ms.kinorent.dto.request.MovieDto;
import com.geniusee.ms.kinorent.dto.request.OrderDetailsRequestDto;
import com.geniusee.ms.kinorent.dto.response.OrderCreationResponseDto;
import com.geniusee.ms.kinorent.dto.response.OrdersStatusResponseDto;
import com.geniusee.ms.kinorent.entity.OrdersBulk;
import com.geniusee.ms.kinorent.exception.BadRequestException;
import com.geniusee.ms.kinorent.exception.NotFoundException;
import com.geniusee.ms.kinorent.logger.MainLogger;
import com.geniusee.ms.kinorent.mapper.OrderCreationMapper;
import com.geniusee.ms.kinorent.messagefactory.MessageProducer;
import com.geniusee.ms.kinorent.repository.OrdersBulkRepository;
import com.geniusee.ms.kinorent.service.OrderCreationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderCreationServiceImpl implements OrderCreationService {
    private static final MainLogger LOGGER = MainLogger.getLogger(OrderCreationServiceImpl.class);
    private final MessageProducer messageProducer;
    private final OrdersBulkRepository ordersBulkRepository;
    private final OrderCreationMapper orderCreationMapper;

    @Override
    public OrderCreationResponseDto createBulkOrders(OrderDetailsRequestDto orderDetailsRequestDto,
                                                     String userId) {

        if (containsDuplicateMovie(orderDetailsRequestDto)) {
            BadRequestException.throwItWithMessage("Request contains duplicate movie");
        }

        var orderReferenceId = orderDetailsRequestDto.getOrderReferenceId();
        var bulk = getBulkByOrderReferenceId(orderReferenceId);

        if (bulk.isEmpty()) {
            saveInitialBulk(orderDetailsRequestDto, userId);
        }

        produceBulkToKafka(orderDetailsRequestDto);
        return OrderCreationResponseDto.of(OperationStatus.SUCCESS);
    }

    private boolean containsDuplicateMovie(OrderDetailsRequestDto movieRequestDto) {

        return movieRequestDto.getMovieDtos().stream()
                .map(MovieDto::getTitle).count()
                != movieRequestDto.getMovieDtos().stream()
                .map(MovieDto::getTitle)
                .collect(Collectors.toSet()).size();
    }

    private void saveInitialBulk(OrderDetailsRequestDto orderDetailsRequestDto, String userId) {
        orderDetailsRequestDto.getMovieDtos().stream()
                .map(orderCreationMapper::toBulk)
                .collect(Collectors.toSet())
                .forEach(bulkEntry -> {
                    bulkEntry.setUserId(userId);
                    bulkEntry.setCreateTime(LocalDateTime.now());
                    bulkEntry.setOrderReferenceId(orderDetailsRequestDto.getOrderReferenceId());
                    saveBulk(bulkEntry);
                });
    }

    private void produceBulkToKafka(OrderDetailsRequestDto orderDetailsRequestDto) {
        var orderReferenceId = orderDetailsRequestDto.getOrderReferenceId();
        var bulk = getBulkByOrderReferenceId(orderReferenceId);

        bulk.forEach(bulkEntry -> {
            orderCreationMapper.enrichBulkEntryWithRequestDetails(orderDetailsRequestDto, bulkEntry);
            updateExistingBulkEntry(bulkEntry);
        });

        orderDetailsRequestDto
                .getMovieDtos()
                .forEach(msgDetails ->
                        messageProducer.produceMessage(msgDetails, orderReferenceId));
    }

    @Override
    public OrdersStatusResponseDto consumeCreatedOrders(String orderReferenceId) {
        var bulk = getBulkByOrderReferenceId(orderReferenceId);

        if (bulk.isEmpty()) {
            throw new NotFoundException(ExceptionMessage.NOT_FOUND);
        }

        return OrdersStatusResponseDto.of(orderCreationMapper.toStatusDtoList(bulk));
    }

    private void updateExistingBulkEntry(OrdersBulk bulk) {
        var bulkId = bulk.getId();

        ordersBulkRepository.findById(bulkId).ifPresentOrElse(value -> saveBulk(bulk),
                () -> LOGGER.error("Updating table has failed"));
    }

    private List<OrdersBulk> getBulkByOrderReferenceId(String orderReferenceId) {
        try {
            return ordersBulkRepository.findAllByOrderReferenceId(orderReferenceId);
        } catch (Exception ex) {
            LOGGER.error("Getting bulk from database by orderReferenceId has failed",
                    ex.getMessage());
            return Collections.emptyList();
        }
    }

    private void saveBulk(OrdersBulk bulk) {
        try {
            ordersBulkRepository.save(bulk);
        } catch (Exception ex) {
            LOGGER.error("Saving to the table has failed due to: ", ex.getMessage());
        }
    }

}

