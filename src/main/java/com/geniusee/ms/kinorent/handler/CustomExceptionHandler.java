package com.geniusee.ms.kinorent.handler;

import com.geniusee.ms.kinorent.constant.ExceptionMessage;
import com.geniusee.ms.kinorent.dto.ErrorResponseDto;
import com.geniusee.ms.kinorent.exception.BadRequestException;
import com.geniusee.ms.kinorent.exception.MessageProductionException;
import com.geniusee.ms.kinorent.exception.NotFoundException;
import com.geniusee.ms.kinorent.exception.RetryProcessException;
import com.geniusee.ms.kinorent.logger.MainLogger;
import io.micrometer.core.lang.NonNullApi;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@ControllerAdvice
@RequiredArgsConstructor
@NonNullApi
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    private static final MainLogger MAIN_LOGGER = MainLogger.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleExceptions(Exception ex, WebRequest request) {
        return buildResponseEntity(ex, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        var errorDetails = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(", "));

        return getResponseEntity(ex, status, request, errorDetails, ExceptionMessage.INPUT_VALIDATION);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return getResponseEntity(ex, status, request, ex.getMessage(), ExceptionMessage.INTERNAL_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return getResponseEntity(ex, status, request, ex.getMessage(), ExceptionMessage.METHOD_NOT_SUPPORTED);
    }

    private ResponseEntity<Object> buildResponseEntity(Exception ex, WebRequest request) {
        if (ex instanceof ConstraintViolationException) {

            String errorDetail = ((ConstraintViolationException) ex).getConstraintViolations()
                    .stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.joining(", "));
            return getResponseEntity(ex, HttpStatus.BAD_REQUEST, request, errorDetail,
                    ExceptionMessage.INPUT_VALIDATION);

        } else if (ex instanceof BadRequestException) {
            return getResponseEntity(ex, HttpStatus.BAD_REQUEST, request, ex.getMessage(),
                    ExceptionMessage.BAD_REQUEST_EXCEPTION);
        } else if (ex instanceof MessageProductionException) {
            return getResponseEntity(ex, HttpStatus.INTERNAL_SERVER_ERROR, request, ex.getMessage(),
                    ExceptionMessage.INTERNAL_KAFKA_ERROR);

        } else if (ex instanceof RetryProcessException) {
            return getResponseEntity(ex, HttpStatus.ACCEPTED, request, ex.getMessage(),
                    ExceptionMessage.INTERNAL_KAFKA_ERROR);

        } else if (ex instanceof NotFoundException) {
            return getResponseEntity(ex, HttpStatus.NOT_FOUND, request, ex.getMessage(), ExceptionMessage.NOT_FOUND);
        } else {
            return getResponseEntity(ex, HttpStatus.INTERNAL_SERVER_ERROR, request, ex.getMessage(),
                    ExceptionMessage.INTERNAL_ERROR);
        }
    }

    private ResponseEntity<Object> getResponseEntity(
            Exception ex, HttpStatus status, WebRequest request, String errorDetail, String message) {
        MAIN_LOGGER.error("{} : {} : {}", ex.getClass(), ex.getMessage(), ex.getStackTrace());
        var errorResponseDto = ErrorResponseDto.builder()
                .status(status.value())
                .error(status.getReasonPhrase())
                .message(message)
                .errorDetail(errorDetail)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(LocalDateTime.now())
                .build();

        return new ResponseEntity<>(errorResponseDto, status);
    }

}
