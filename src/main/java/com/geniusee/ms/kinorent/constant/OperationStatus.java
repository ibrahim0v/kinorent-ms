package com.geniusee.ms.kinorent.constant;

public final class OperationStatus {
    public static final String SUCCESS = "Request has been sent to the Kafka for order creation";
    public static final String IN_PROGRESS = "IN_PROGRESS";
    public static final String ERROR = "ERROR";

    private OperationStatus() {
    }
}
