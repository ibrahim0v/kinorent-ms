package com.geniusee.ms.kinorent.constant;

public final class ExceptionMessage {
    public static final String INPUT_VALIDATION = "Input validation has been failed";
    public static final String INTERNAL_ERROR = "Internal error has been occurred";
    public static final String INTERNAL_KAFKA_ERROR = "Producing to the kafka has failed";
    public static final String METHOD_NOT_SUPPORTED = "Method is not supported";
    public static final String BAD_REQUEST_EXCEPTION = "Request body validation has failed due to some reasons";
    public static final String MSG_PROD_FAIL = "Message production has failed due to database error";
    public static final String NOT_FOUND = "No records found for this particular orderReferenceId";

    private ExceptionMessage() {
    }
}
