package com.geniusee.ms.kinorent.controller;

import com.geniusee.ms.kinorent.dto.request.OrderDetailsRequestDto;
import com.geniusee.ms.kinorent.dto.response.OrderCreationResponseDto;
import com.geniusee.ms.kinorent.dto.response.OrdersStatusResponseDto;
import com.geniusee.ms.kinorent.service.OrderCreationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Size;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
@Api(tags = "Order controller to rent movies")
@Validated
public class OrderController {
    private final OrderCreationService orderCreationService;

    @ApiOperation(value = "Create Order",
            notes = "Creates movie orders in a bulk or single way over Kafka",
            response = OrderCreationResponseDto.class)

    @PostMapping("/bulk-orders")
    public OrderCreationResponseDto createBulkOrders(
            @Size(max = 10, message = "{invalid.userId.size}")
            @RequestHeader(value = "UserId") String userId,
            @ApiParam(value = "Details for creating order", required = true)
            @RequestBody @Valid OrderDetailsRequestDto movieRequestDto) {
        return orderCreationService.createBulkOrders(movieRequestDto, userId);
    }

    @ApiOperation(value = "Returns information about bulk movie orders operation; Status of each orders",
            response = OrderCreationResponseDto.class)
    @GetMapping("/orders-status")
    public ResponseEntity<OrdersStatusResponseDto> getOrdersCreationStatus(
            @RequestParam String orderReferenceId) {
        var result = orderCreationService.consumeCreatedOrders(orderReferenceId);
        return ResponseEntity.ok(result);
    }
}
