package com.geniusee.ms.kinorent.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderStatusDto {
    private String kafkaReferenceId;
    private String title;
    private String status;
}
