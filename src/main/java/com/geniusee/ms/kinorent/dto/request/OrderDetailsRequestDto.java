package com.geniusee.ms.kinorent.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class OrderDetailsRequestDto {
    @NotBlank(message = "{invalid.orderReferenceId.blank}")
    @Size(max = 20, message = "{invalid.orderReferenceId.size}")
    private String orderReferenceId;

    @NotNull(message = "{invalid.movie.blank}")
    @NotEmpty(message = "{invalid.movie.empty}")
    @Valid
    private Set<MovieDto> movieDtos;
}
