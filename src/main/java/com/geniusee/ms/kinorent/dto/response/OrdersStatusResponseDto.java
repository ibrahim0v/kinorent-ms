package com.geniusee.ms.kinorent.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrdersStatusResponseDto {
    List<OrderStatusDto> orderStatusDtos;

    public static OrdersStatusResponseDto of(List<OrderStatusDto> orderStatusDtos) {
        return new OrdersStatusResponseDto(orderStatusDtos);
    }
}
