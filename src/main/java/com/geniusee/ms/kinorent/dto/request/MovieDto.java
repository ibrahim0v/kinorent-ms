package com.geniusee.ms.kinorent.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class MovieDto {

    //TODO Implement these input constraint validations if it's required

//    @NotBlank(message = "{invalid.title.blank}")
//    @Size(max = 7, message = "{invalid.length.title}")
    private String title;

//    @NotBlank(message = "{invalid.year.blank}")
    private String year;

//    @NotBlank(message = "{invalid.genre.blank}")
    private String genre;

//    @NotBlank(message = "{invalid.price.blank}")
    private Double price;

//    @NotBlank(message = "{invalid.language.blank}")
    private String language;

//    @NotBlank(message = "{invalid.director.blank}")
    private String director;

//    @NotBlank(message = "{invalid.rating.blank}")
    private String rating;
}
