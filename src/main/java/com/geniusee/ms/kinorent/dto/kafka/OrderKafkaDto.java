package com.geniusee.ms.kinorent.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderKafkaDto {
    private String referenceId;
    private OrderDetailsKafkaDto orderDetailsKafkaDto;

    public static OrderKafkaDto of(String referenceId, OrderDetailsKafkaDto orderDetailsKafkaDto) {
        return new OrderKafkaDto(referenceId, orderDetailsKafkaDto);
    }
}
