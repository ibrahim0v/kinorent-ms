package com.geniusee.ms.kinorent.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailsKafkaDto {
    private String title;
    private String year;
    private String genre;
    private Double price;
    private String language;
    private String director;
    private String rating;
}
