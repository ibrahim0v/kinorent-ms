package com.geniusee.ms.kinorent.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class OrderCreationResponseDto {
    private String operationStatus;

    public static OrderCreationResponseDto of(String operationStatus) {
        return new OrderCreationResponseDto(operationStatus);
    }
}

