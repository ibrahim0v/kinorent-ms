
CREATE TABLE IF NOT EXISTS public.ms_kinorent_orders
(
    id SERIAL NOT NULL,
    order_reference_id text COLLATE pg_catalog."default" NOT NULL,
    kafka_reference_id text COLLATE pg_catalog."default",
    title text COLLATE pg_catalog."default",
    user_id text COLLATE pg_catalog."default",
    order_details text COLLATE pg_catalog."default",
    status text COLLATE pg_catalog."default",
    create_time timestamp with time zone,
    CONSTRAINT ms_kinorent_orders_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;