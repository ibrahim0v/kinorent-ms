# kinorent-ms

# kinorent-ms

Kinorent is a spring boot application for dealing with orders of movies. This microservice contains REST API with a controller consists two methods (POST,GET)

With POST Method you can insert multiple movie orders.
You have to provide userId and the orderReferenceId and the movies. Title of movies have to be distinct.
And with same orderReferenceId you can check the status of each orders.

You can check swagger documentation for further information when you run the application

```bash
http://localhost:8080/kinorent-ms/swagger-ui.html
```

## Installation

Clone the source code


```bash
git clone https://gitlab.com/ibrahim0v/kinorent-ms.git
```

Download and install Apache Kafka from here

```bash
https://www.apache.org/dyn/closer.lua/zookeeper/zookeeper-3.7.0/apache-zookeeper-3.7.0-bin.tar.gz
```
Download and install Apache zookeeper from here

```bash
https://archive.apache.org/dist/kafka/2.8.0/kafka_2.12-2.8.0.tgz
```

You will need postgres server for liquibase to make the schema


```bash
https://www.postgresql.org/download/
```

## Example

Use gradle to build and run the application

```bash
gradle build
```


```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)